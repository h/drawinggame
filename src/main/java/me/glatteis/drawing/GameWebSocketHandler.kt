package me.glatteis.drawing

import org.eclipse.jetty.websocket.api.Session
import org.eclipse.jetty.websocket.api.WebSocketException
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage
import org.eclipse.jetty.websocket.api.annotations.WebSocket
import org.json.JSONObject
import java.util.concurrent.ConcurrentHashMap


// Handles all WebSockets. There is only one instance per server
@WebSocket
object GameWebSocketHandler {

    // Connection between username and game ID as used in sessions
    private data class UserSession(val gameId: String, val username: String)

    // Links WebSocket session to game & user
    private val sessions = ConcurrentHashMap<Session, UserSession>()

    @OnWebSocketClose
    fun onClose(session: Session, a: Int, b: String) {
        // Get data of that session
        val userSession = sessions[session] ?: return
        val game = games[userSession.gameId] ?: return
        val users = game.userSessions.keys
        val user = with(game) {
            session.getUser()
        } ?: return
        // Remove from users in game, guesses in game, global sessions
        users.remove(user)
        sessions.remove(session)

        // Send user update to all users in game
        sendToAll(game.userSessions.values, JSONObject(
                mapOf(
                        Pair("users", users)
                )
        ))
    }

    @OnWebSocketMessage
    fun onMessage(session: Session, message: String) {
        // Parse message in JSON
        val jsonObject = JSONObject(message)
        // Session wants to connect
        if (jsonObject["type"] == "connect") {
            // Get game ID and username
            var username = (jsonObject["username"] ?: return) as String
            val id = (jsonObject["id"] ?: return) as String
            // Put into sessions map
            sessions.put(session, UserSession(id, username))
            val game = games[id] ?: return
            val users = game.userSessions.keys
            // If this user was already online before and has simply reloaded the page,
            // transfer their points and keep their old name, remove old user
            var points = 0
            val usersToRemove = HashSet<User>()
            do {
                val usernameBefore = username
                for (u in users) {
                    if (u.name == username) {
                        val thisUserSession = game.userSessions[u] ?: continue
                        // Sending stay alive packet. If this fails, the session will be closed by the
                        // sendMessage function
                        GameWebSocketHandler.sendMessage(thisUserSession, JSONObject(mapOf("staying" to "alive")))
                        if (thisUserSession.isOpen) {
                            username += "~"
                        } else {
                            usersToRemove.add(u)
                            thisUserSession.close()
                            game.userSessions.remove(u)
                            points = u.points
                        }
                    }
                }
            } while (usernameBefore != username)
            for (u in usersToRemove) {
                users.remove(u)
            }
            if (usersToRemove.isEmpty() && game.hasStarted) {
                // if spectator function is ever added, this is where spectators would be initialized
            }
            // Create user
            val user = User(username, points)
            game.userSessions.put(user, session)
            game.onJoin(session)
        } else {
            // If message is not "connect", send it for the game to handle
            val gameId = sessions[session]?.gameId ?: return
            val game = games[gameId] ?: return
            game.message(jsonObject, session)
        }
    }

    fun sendMessage(session: Session, message: JSONObject) {
        if (session.isOpen) {
            try {
                session.remote.sendString(message.toString())
            } catch (e: WebSocketException) {
                session.close()
            }
        }

    }

    fun sendToAll(sessions: Iterable<Session>, message: JSONObject) {
        sessions.forEach {
            sendMessage(it, message)
        }
    }

}