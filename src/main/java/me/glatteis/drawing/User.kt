package me.glatteis.drawing

// Represents in-game player
class User(val name: String, var points: Int)