package me.glatteis.drawing

import org.eclipse.jetty.websocket.api.Session
import org.json.JSONObject
import spark.ModelAndView
import spark.Request
import spark.Response
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.ArrayList
import kotlin.concurrent.timer

/**
 * Created by Linus on 16.08.2017!
 */
class Game(val name: String, val id: String, val countdownTime: Int?, val winPoints: Int?, val isPublic: Boolean,
           val isCustom: Boolean) {

    /*
    An instance of this class represents a running game.

    Do differentiate it from a Spark Session, the WebSocket session is imported as "JSession"
     */

    // The OP is the user that controls this game.
    var op: User? = null

    // Reference to WebSocket sessions from User. User object can remain while Session can change, for instance
    // if the user reloads the page.
    val userSessions = ConcurrentHashMap<User, Session>()

    // This is the order that turns will be in.
    private val turnOrder = ArrayList<User>()

    var userOnTurn: User? = null

    var word: DrawingWord? = null

    var countdown = countdownTime ?: -1
    val countdownTimer: Timer? = if (countdownTime == null) null else timer(period = 1000L) {
        if (!hasStarted || userSessions.isEmpty()) return@timer
        if (countdown > 0) {
            countdown--
            val nonNullWord = word
            if (nonNullWord != null &&
                    (nonNullWord.hintedLetters.toDouble() / nonNullWord.word.length) * countdownTime * 2
                            < countdownTime - countdown) {
                nonNullWord.newHintLetter()
            }
            GameWebSocketHandler.sendToAll(userSessions.values, JSONObject(
                    mapOf(
                            "type" to "updateCountdown",
                            "countdown" to countdown,
                            "hint" to word?.asHint()
                    )
            ))
        } else {
            nextTurn()
        }
    }

    var hasStarted = false // If the game has started, users will no longer be able to join and play

    // This is the accumulation of every packet that has been sent within the current round, defining the image.
    // If someone joins they will be sent all of these packets.
    var currentImage = ArrayList<JSONObject>()

    private fun checkGuess(guess: String) : Boolean {
        val nonNullWord = word ?: return false
        if (guess.isBlank()) return false
        return guess.trim().equals(nonNullWord.word, ignoreCase = true) ||
                nonNullWord.synonyms.any {
                    guess.equals(it, ignoreCase = true)
                }
    }

    fun Session.getUser(): User? {
        for ((u, s) in userSessions) {
            if (this == s) {
                return u
            }
        }
        return null
    }

    // Called when a new player is on turn. Initializes their turn.
    fun nextTurn() {
        if (countdownTime != null) {
            countdown = countdownTime + 1
        }
        if (turnOrder.size != userSessions.size) {
            val usersNotInTurnOrder = ArrayList<User>()
            for ((u, _) in userSessions) {
                if (!turnOrder.contains(u)) {
                    usersNotInTurnOrder += u
                }
            }
            turnOrder += usersNotInTurnOrder
            val usersFalselyInTurnOrder = ArrayList<User>()
            for (u in turnOrder) {
                if (!userSessions.containsKey(u)) {
                    usersFalselyInTurnOrder += u
                }
            }
            for (u in usersFalselyInTurnOrder) {
                userSessions.remove(u)
            }
        }

        word = wordList[(Math.random() * wordList.size).toInt()]

        val turnOrderIndex = turnOrder.indexOf(userOnTurn ?: op)
        userOnTurn = turnOrder[(turnOrderIndex + 1) % turnOrder.size]
        GameWebSocketHandler.sendToAll(userSessions.values, JSONObject(
                mapOf(
                        "type" to "newRound",
                        "userOnTurn" to userOnTurn?.name,
                        "users" to userSessions.keys
                )
        ))

        GameWebSocketHandler.sendMessage(userSessions[userOnTurn ?: return] ?: return, JSONObject(
                mapOf(
                        "word" to word?.word,
                        "synonyms" to word?.synonyms?.joinToString(", ")?.dropLast(2)
                )
        ))
    }


    init {
        // Timer that looks for closed sockets every 10 seconds
        timer(daemon = true, initialDelay = 10000, period = 10000) {
            // Close & remove all sessions that are not open anymore
            val toRemove = HashSet<Pair<User, Session>>()
            for ((u, s) in userSessions) {
                if (!s.isOpen) {
                    toRemove.add(Pair(u, s))
                }
            }
            for ((u, s) in toRemove) {
                println("Removing $u")
                if (s.isOpen) s.close()
                userSessions.remove(u)
                if (u == op) {
                    op = userSessions.keys.first()
                    GameWebSocketHandler.sendToAll(userSessions.values, JSONObject(
                            mapOf(
                                    "op" to op?.name
                            )
                    ))
                }
                if (u == userOnTurn) {
                    nextTurn()
                }
            }

            if (userSessions.isEmpty()) {
                // If there are no users anymore, remove the game from games and cancel this timer
                games.remove(id)
                this.cancel()
            } else if (toRemove.isNotEmpty()) {
                // Else, update everyone with the remaining set of users
                for ((u, s) in userSessions.entries) {
                    GameWebSocketHandler.sendMessage(s, JSONObject(
                            mapOf(
                                    "users" to userSessions.keys,
                                    "username" to u.name
                            )
                    ))
                }
            }
        }
    }

    // Called after a user sends the join packet through the WebSocket after joining
    fun onJoin(session: Session) {
        if (op == null || op !in userSessions.keys) {
            op = session.getUser()
        }
        GameWebSocketHandler.sendMessage(session, JSONObject(
                mapOf(
                        "op" to op?.name
                ))
        )
        for ((u, s) in userSessions.entries) {
            GameWebSocketHandler.sendMessage(s, JSONObject(
                    mapOf(
                            "users" to userSessions.keys,
                            "username" to u.name
                    )
            ))
        }

        if (hasStarted) {
            GameWebSocketHandler.sendMessage(session, JSONObject(
                    mapOf(
                            "userOnTurn" to userOnTurn?.name
                    )
            ))
            if (session.getUser() == userOnTurn) {
                GameWebSocketHandler.sendMessage(userSessions[userOnTurn ?: return] ?: return, JSONObject(
                        mapOf(
                                "word" to word?.word,
                                "synonyms" to word?.synonyms?.joinToString(", ")?.dropLast(2)
                        )
                ))
            }
        }
    }

    // Called when a user sends a packet through the WebSocket
    fun message(message: JSONObject, session: Session) {
        val user = session.getUser()
        when (message["type"]) {
            "start" -> {
                if (user == op) {
                    hasStarted = true
                    GameWebSocketHandler.sendToAll(userSessions.values, JSONObject(
                            mapOf(
                                    "type" to "start",
                                    "users" to userSessions.keys
                            )
                    ))
                    nextTurn()
                }
            }

            "drawAction" -> {
                if (user != userOnTurn) return
                val action = message["action"] ?: return
                val tool = message["tool"] ?: return

                val packet = JSONObject(
                        mapOf(
                                "type" to "drawAction",
                                "action" to action,
                                "tool" to tool
                        )
                )
                currentImage.add(packet)

                GameWebSocketHandler.sendToAll(userSessions.values.filter { it != session }, packet)
            }

            "chatMessage" -> {
                // Scoop out user, message
                user ?: return
                val chatMessage = message["message"] ?: return
                if (chatMessage !is String || chatMessage.isBlank()) return

                if (chatMessage.startsWith("/")) {
                    if (chatMessage.startsWith("/start")) {
                        if (user == op) {
                            hasStarted = true
                            nextTurn()
                        }
                    }
                }

                //Send message to every user
                sendChatMessage(user.name,
                        chatMessage
                                .replace("<", "")
                                .replace(">", "")
                )
            }

            "guess" -> {
                if (user == null || user == userOnTurn) return
                val guess = (message["guess"] ?: return)
                guess as? String ?: return
                sendChatMessage(user.name, guess + "??")
                if (checkGuess(guess)) {
                    GameWebSocketHandler.sendToAll(userSessions.values, JSONObject(
                            mapOf(
                                    "type" to "playerWon",
                                    "winningPlayer" to user.name
                            )
                    ))
                    user.points++
                    nextTurn()
                }
            }

            "canvas-ready" -> {
                if (!currentImage.isEmpty()) {
                    val image = currentImage.toTypedArray()
                    for (packet in image) {
                        GameWebSocketHandler.sendMessage(session, packet)
                    }
                }
            }
        }
    }

    // This function is called whenever requests the response from /game with id of this game
    fun getGame(request: Request, response: Response): ModelAndView {
        // If the requesting player does not yet have a username, send to choose_username
        val username: String? = request.session().attribute("username")
        if (username == null || username.isBlank()) {
            response.redirect("/choose_name?id=" + id)
        }
        // Add attributes to HTML
        val attributes = mapOf(
                "sys_username" to username,
                "sys_game_name" to name,
                "sys_game_id" to id,
                "sys_points_to_win" to winPoints,
                "sys_is_public" to isPublic,
                "sys_chill_mode" to (countdownTime == null),
                "sys_user_on_turn" to userOnTurn?.name,
                "sys_has_started" to hasStarted
        )
        request.session().attribute("redirect_id", id)
        return ModelAndView(attributes + localizations[request.session().getLanguage()]!!,
                filePrefix + "game.html")
    }

    private fun sendChatMessage(author: String, message: String) {
        GameWebSocketHandler.sendToAll(userSessions.values, JSONObject(
                mapOf(
                        "type" to "chatMessage",
                        "message" to message,
                        "senderName" to author
                )
        ))
    }

}