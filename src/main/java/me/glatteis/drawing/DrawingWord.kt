package me.glatteis.drawing

data class DrawingWord(val word: String, val synonyms: List<String>) {
    // Number of exposed letters
    var hintedLetters = 0
    // This array stores the letters that are already exposed in the hint as true
    private val hint = Array(word.length) {
        false
    }

    // Returns the word with hint letters exposed and non-exposed letters as _
    fun asHint(): String {
        var wordWithHint = ""
        word.toCharArray().forEachIndexed { i, char ->
            wordWithHint += if (hint[i]) {
                char
            } else {
                "_"
            }
        }
        return wordWithHint.toCharArray().joinToString(" ")
    }

    // Expose a new hint letter
    fun newHintLetter() {
        hintedLetters++
        var index: Int
        do {
            index = (Math.random() * hint.size).toInt()
        } while (hint[index])
        hint[index] = true
    }
}