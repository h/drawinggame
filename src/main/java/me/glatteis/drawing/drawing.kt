package me.glatteis.drawing

import com.esotericsoftware.yamlbeans.YamlReader
import org.json.JSONArray
import spark.ModelAndView
import spark.kotlin.Http
import spark.kotlin.ignite
import spark.template.mustache.MustacheTemplateEngine
import java.io.File
import java.io.FileReader
import java.security.SecureRandom
import java.util.concurrent.ConcurrentHashMap


/**
 * Created by Linus on 16.08.2017!
 */

val mustacheTemplateEngine = MustacheTemplateEngine()

// Generates game IDs
private val randomStringGenerator = RandomStringGenerator(SecureRandom())

// The file prefix for resources
var filePrefix: String = ""

// Game ID to Game map
val games = ConcurrentHashMap<String, Game>()

// List of possible words todo localize this in different languages
val wordList = ArrayList<DrawingWord>()

// Available languages and their translations
val localizations = HashMap<String, Map<String, String>>()
val languages = listOf("de", "en")

fun main(args: Array<String>) {
    // Init spark server
    val http: Http = ignite()
    if (args.isEmpty()) throw IllegalArgumentException("Port has to be specified.")
    val portAsString = args[0]
    val port = portAsString.toInt()
    http.port(port)
    http.service.webSocket("/gamesocket", GameWebSocketHandler)

    // Set resource path
    if (args.size > 1) {
        filePrefix = args[1]
        http.staticFiles.location("/app/${args[1]}")
    } else {
        http.staticFiles.location("/")
    }

    // Load words
    val listOfWords = File("word_list.txt").readText().split("\n")
    for (w in listOfWords) {
        val synonyms = w.split(";")
        wordList += DrawingWord(synonyms[0], synonyms.subList(1, synonyms.size))
    }

    // Load localizations
    for (language in languages) {
        val reader = YamlReader(FileReader("$language.yaml"))
        val obj = reader.read()
        @Suppress("UNCHECKED_CAST")
        localizations[language] = obj as Map<String, String>
    }

    // Serve start.html as front page
    http.get("/") {
        mustacheTemplateEngine.render(
                ModelAndView(localizations[session().getLanguage()]!!, filePrefix + "start.html")
        )
    }

    // Server create.html as game creation page
    http.get("/create") {
        val attributes = HashMap<String, Any>()
        mustacheTemplateEngine.render(
                ModelAndView(attributes + localizations[session().getLanguage()]!!, filePrefix + "create_game.html")
        )
    }
    // Create a game
    http.post("/create") {
        // Fetch params
        val gameName = queryParams("game_name")
        val username = queryParams("username")
        val timeAsString = queryParams("countdown_time")
        val pointsToWinAsString = queryParams("points_to_win")
        val publicAsString = queryParams("public")
        // A public game will be visible from the game browser
        val isPublic = (publicAsString == "on")
        // Check for invalid data
        if (gameName.isBlank() || username.isBlank() || username.length > 16 || gameName.length > 16) {
            response.redirect("/")
            return@post "Invalid parameters."
        }
        // Try to convert time and points to win values to numbers. If not successful, abort
        val time: Int?
        val pointsToWin: Int?
        try {
            time = if (timeAsString.isBlank()) null else timeAsString.toInt()
            println(pointsToWinAsString)
            pointsToWin = if (pointsToWinAsString.isBlank()) null else pointsToWinAsString.toInt()
        } catch (e: Exception) {
            response.redirect("/")
            return@post "Invalid parameters."
        }
        val isCustom = time == null
        // Check for invalid data
        if ((time != null && time < 10) || (pointsToWin != null && pointsToWin < 1)) {
            response.redirect("/")
            return@post "Invalid parameters."
        }
        // Games will be identified using unique IDs
        var id: String
        do {
            id = randomStringGenerator.randomString(8)
        } while (games.containsKey(id))
        // Create game instance
        val game = Game(gameName, id, time, pointsToWin, isPublic, isCustom)
        games.put(id, game)
        // Add username to session
        request.session(true).attribute("username", username)
        // Redirect
        response.redirect("/game?id=" + id)
    }
    http.get("/game") {
        val id = request.queryParams("id") ?:
                return@get "You did not specify an id. <a href='/'>Back to drawing</a>"
        val game = games[id] ?:
                return@get "There's no game of that id. <a href='/'>Back to drawing</a>"
        mustacheTemplateEngine.render(
                game.getGame(request, response)
        )
    }
    http.get("/list") {
        // For every game: Get data if public
        val publicGames = games.values
                .filter { it.isPublic }
                .map {
                    mapOf(
                            "name" to it.name,
                            "players" to it.userSessions.size,
                            "pointsToWin" to it.winPoints,
                            "countdownTime" to it.countdownTime,
                            "id" to it.id,
                            "hasStarted" to it.hasStarted
                    )
                }
        // Pass to JS on page as attribute, script will build table
        val attributes = HashMap<String, Any>()
        attributes["sys_games"] = JSONArray(publicGames)
        mustacheTemplateEngine.render(
                ModelAndView(attributes + localizations[session().getLanguage()]!!, filePrefix + "list_games.html")
        )
    }
    http.get("/choose_name") {
        // This is the game ID that the server will redirect to after choosing name
        val id: String? = queryParams("id")
        if (id == null || id.isBlank()) {
            return@get "Error: ID is invalid. Please try again."
        }
        request.session().attribute("redirect_id", id)
        mustacheTemplateEngine.render(
                ModelAndView(localizations[session().getLanguage()]!!, filePrefix + "choose_name.html")
        )
    }
    // After choosing a name, data will end up in session
    http.post("/choose_name") {
        val id: String? = request.session().attribute("redirect_id")
        val name: String? = request.queryParams("username")
        if (id != null && name != null && name.isNotBlank() && name.length <= 16) {
            request.session().attribute("username", name)
            // Redirect to game
            response.redirect("/game?id=" + id)
        }
    }
    http.post("/toggle_language") {
        if (languages.contains(request.body())) {
            session().attribute("language", request.body())
        }
    }
}

fun spark.Session.getLanguage(): String {
    if (attributes().contains("language")) {
        return attribute("language")
    }
    return "de"
}