if (pointsToWin === "") {
    $("#points_to_win_text").hide()
}
// Returns if this browser is the turning player right now
function isOnTurn() {
    return username === userOnTurn;
}

$("#start_button").hide();
$("#tools").hide();

var webSocket;

appendChatMessage("drawing", messages.welcomeText);

// Init WebSocket
var websocketProtocol = "wss";
if (location.protocol !== "https:") {
    websocketProtocol = "ws";
}
webSocket = new WebSocket(websocketProtocol +
    "://" + location.hostname + ":" + location.port + "/gamesocket");
webSocket.onopen = function () {
    webSocket.send(JSON.stringify(
        {type: "connect", id: id, username: username}
    ));
};
webSocket.onmessage = webSocketOnMessage;

function updateHtmlUserTable(users) {
    const htmlUserTable = $("#user_table")[0];
    htmlUserTable.innerHTML = "<thead><tr><th>name</th><th>#</th></tr></thead>";
    users.sort(function (a, b) {
        return b.points - a.points
    });
    for (var i = 0; i < users.length; i++) {
        const user = users[i];
        var name = user.name;
        if (user.name === username) {
            name += " *";
        }
        if (user.name === op) {
            name += " [OP]"
        }
        if (user.name === userOnTurn) {
            name += "  <span class=\"glyphicon\">&#x270f;</span>"
        }
        htmlUserTable.innerHTML += "<tr><td>" + name + (user.hasGuessed ? " (g)" : "") + "</td><td>" + user.points.toString() + "</td></tr>";
    }
}

function webSocketOnMessage(message) {
    var data = JSON.parse(message.data);
    if (data.username !== undefined) {
        username = data.username;
    }
    if (data.op !== undefined) {
        op = data.op;
        if (username === op) {
            if (hasStarted === "true") {
                $("#start_button").hide();
            } else {
                $("#start_button").show();
            }
            appendChatMessage("drawing", messages.youAreNowOp);
        }
    }
    if (data.userOnTurn !== undefined) {
        userOnTurn = data.userOnTurn;
        if (userOnTurn === username) {
            appendChatMessage("drawing", messages.youAreOnTurn);
            $("#guess_form").hide();
        } else {
            appendChatMessage("drawing", messages.isOnTurn.replace("[u]", userOnTurn));
            $("#guess_form").show();
        }
    }
    if (data.users !== undefined) {
        updateHtmlUserTable(data.users);
    }
    if (data.word !== undefined) {
        $("#word")[0].innerHTML = data.word;
        $("#word_synonyms")[0].innerHTML = data.synonyms;
    }
    if (data.type === "updateCountdown") {
        $("#countdown_label")[0].innerHTML = data.countdown.toString();
        setHint(data.hint);
    } else if (data.type === "chatMessage") {
        const chatMessage = data.message;
        var dataUsername = data.senderName;
        if (data.senderName === username) {
            dataUsername += " *";
        }
        if (data.senderName === userOnTurn) {
            dataUsername += " <span class=\"glyphicon\">&#x270f;</span>"
        }
        if (data.senderName === op) {
            dataUsername += " [OP]"
        }
        appendChatMessage(dataUsername, chatMessage)
    } else if (data.type === "drawAction") {
        receiveAction(data.action, data.tool)
    } else if (data.type === "newRound") {
        clearCanvas();
    } else if (data.type === "playerWon") {
        const winningPlayer = data["winningPlayer"];
        appendChatMessage("montagsmaler", winningPlayer + " hat diese Runde gewonnen!")
    } else if (data.type === "start") {
        hasStarted = true;
        window.clearTimeout(gameOfLifeTimer);
        $("#start_button").hide();
    }
    if (userOnTurn === username) {
        $("#tools").show();
    } else {
        $("#tools").hide();
    }
}

function appendChatMessage(username, chatMessage) {
    const $chatWindow = $("#chat_window");
    const chatWindow = $chatWindow[0];
    chatWindow.innerHTML += "<span>" + username + ": " + chatMessage + "</span><br>";
    chatWindow.scrollTop = chatWindow.scrollHeight;
}

function startClicked() {
    if (username === op) {
        webSocket.send(JSON.stringify(
            {"type": "start"}
        ))
    }
}

function setHint(hint) {
    $("#hint_label").text(hint)
}

function guessSubmitted(form) {
    const guess = form.guess.value;
    form.guess.value = null;
    webSocket.send(JSON.stringify(
        {"type": "guess", "guess": guess}
    ));
}

function chatSubmitted() {
    const chatInput = $("#chat_input")[0];
    const message = chatInput.value;
    webSocket.send(JSON.stringify(
        {"type": "chatMessage", "message": message}
    ));
    chatInput.value = "";
    chatInput.focus();
}

