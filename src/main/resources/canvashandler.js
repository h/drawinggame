function sendAction(event) {
    const eventData = new Object();
    eventData.point = event.point;
    eventData.middlePoint = event.middlePoint;
    eventData.delta = event.delta;
    eventData.type = event.type;
    eventData.color = color;
    console.log(eventData);
    try {
        webSocket.send(JSON.stringify(
            {"type": "drawAction", "action": JSON.stringify(eventData), "tool": currentTool}
        ))
    } catch (e) {
        console.log(e);
    }
}

function receiveAction(event, tool) {
    if (tool !== currentTool) {
        switchTool(tool);
    }

    const t = tools[currentTool];
    const recievedEvent = JSON.parse(event);
    const eventAsObject = new Object();
    // I don't understand why JSON doesn't stringify Points like a normal person would
    eventAsObject.point = new Point(recievedEvent.point[1], recievedEvent.point[2]);
    eventAsObject.middlePoint = new Point(recievedEvent.middlePoint[1], recievedEvent.middlePoint[2]);
    eventAsObject.delta = new Point(recievedEvent.delta[1], recievedEvent.delta[2]);
    eventAsObject.type = recievedEvent.type;
    color = recievedEvent.color;
    switch (eventAsObject.type) {
        case "mousedown":
            t.onMouseDownEvent(eventAsObject);
            break;
        case "mousedrag":
            t.onMouseDragEvent(eventAsObject);
            break;
        case "mouseup":
            t.onMouseUpEvent(eventAsObject);
            break;
    }
}

var mouseDown;
var mouseDrag;
var mouseUp;
var currentTool = "pencil";
var tools;
var color = "#000000";
var gameOfLifeTimer = null;

function updateColor(c) {
    color = "#" + c;
}

function onClearClicked() {
    const oldTool = currentTool;
    switchTool("clear");
    const event = Object();
    event.point = new Point(0, 0);
    event.middlePoint = new Point(0, 0);
    event.delta = new Point(0, 0);
    event.type = "mousedown";
    tools[currentTool].onMouseDown(event);
    switchTool(oldTool);
}

window.onload = function () {
    paper.install(window);

    paper.setup('drawing_canvas');

    clearCanvas();

    if (hasStarted === "false") {
        gameOfLifeTimer = window.setInterval(gameOfLife, 500);
        //gameOfLife();
        //gameOfLife();
    }

    const pencil = new Tool();
    const pen = new Tool();
    const eraser = new Tool();
    const clear = new Tool();

    tools = {
        "pencil": pencil,
        "pen": pen,
        "eraser": eraser,
        "clear": clear
    };

    var path;

    for (var i in tools) {
        // noinspection JSUnfilteredForInLoop
        var t = tools[i];
        t.minDistance = 15;
        t.maxDistance = 45;
        t.onMouseDown = function (event) {
            if (!isOnTurn()) return;
            this.onMouseDownEvent(event);
            sendAction(event);
        };
        t.onMouseDrag = function (event) {
            if (!isOnTurn()) return;
            this.onMouseDragEvent(event);
            sendAction(event);
        };
        t.onMouseUp = function (event) {
            if (!isOnTurn()) return;
            this.onMouseUpEvent(event);
            sendAction(event);
        };
    }

    pencil.onMouseDownEvent = function (event) {
        console.log("mousedown");
        path = new Path();
        path.fillColor = color;
        path.add(event.point);
    };

    pencil.onMouseDragEvent = function (event) {
        var step = event.delta.multiply(0.5);
        step = step.divide(step.length);
        step.angle += 90;
        var top = event.middlePoint.add(step);
        var bottom = event.middlePoint.subtract(step);
        path.add(top);
        path.insert(0, bottom);
        path.smooth();
    };

    pencil.onMouseUpEvent = function (event) {
        path.add(event.point);
        path.closed = true;
        path.smooth();
    };

    pen.onMouseDownEvent = function (event) {
        path = new Path();
        path.fillColor = color;
        path.add(event.point);
    };

    pen.onMouseDragEvent = function (event) {
        var step = event.delta.multiply(0.25);
        step.angle += 90;

        var unitStep = step.divide(step.length);

        var top = event.middlePoint.add(unitStep.multiply(10 - step.length / 2));
        var bottom = event.middlePoint.subtract(unitStep.multiply(10 - step.length / 2));

        path.add(top);
        path.insert(0, bottom);
        path.smooth();
    };

    pen.onMouseUpEvent = function (event) {
        path.add(event.point);
        path.closed = true;
        path.smooth();
    };

    eraser.onMouseDownEvent = function (event) {
        path = new Path();
        path.fillColor = "white";
        path.add(event.point);
    };

    eraser.onMouseDragEvent = function (event) {
        var step = event.delta.multiply(0.25);
        step.angle += 90;

        var unitStep = step.divide(step.length);

        console.log(step.length);

        var top = event.middlePoint.add(unitStep.multiply(40 - step.length * 2));
        var bottom = event.middlePoint.subtract(unitStep.multiply(40 - step.length * 2));

        path.add(top);
        path.insert(0, bottom);
        path.smooth();
    };
    eraser.onMouseUpEvent = pen.onMouseUpEvent;

    clear.onMouseDownEvent = function () {
        clearCanvas();
    };
    clear.onMouseDragEvent = function () {
    };
    clear.onMouseUpEvent = function () {
    };

    switchTool(currentTool);

    const sendCanvasMessage = function() {
        webSocket.send(
            JSON.stringify(
                {
                    type: "canvas-ready"
                }
            )
        )
    };

    if (webSocket.readyState === webSocket.OPEN) {
        sendCanvasMessage();
    } else {
        webSocket.addEventListener("open", sendCanvasMessage);
    }
};

function clearCanvas() {
    paper.project.activeLayer.removeChildren();
    paper.view.draw();
}

function switchTool(toolName) {
    currentTool = toolName;
    const tool = tools[toolName];
    mouseDown = tool.onMouseDownEvent;
    mouseDrag = tool.onMouseDragEvent;
    mouseUp = tool.onMouseUpEvent;
    paper.tool = tool;
}

$('input[name="pen-type"]').change(function () {
    switchTool(this.value);
});

function mod(n, m) {
    return ((n % m) + m) % m;
}

var gameOfLifeField = [];
var neighborsArray = [];

function gameOfLife() {
    clearCanvas();
    if (gameOfLifeField.length === 0) {
        for (var x = 0; x < 80; x++) {
            gameOfLifeField[x] = [];
            neighborsArray[x] = [];
            for (var y = 0; y < 50; y++) {
                gameOfLifeField[x][y] = Math.random() < 0.5;
                neighborsArray[x][y] = 0;
            }
        }
    }
    const newField = gameOfLifeField.map(function(n) {
        return n.slice();
    });
    for (var ix = 0; ix < gameOfLifeField.length; ix++) {
        for (var iy = 0; iy < gameOfLifeField[0].length; iy++) {
            var neighbors = 0;
            for (var nx = -1; nx <= 1; nx++) {
                for (var ny = -1; ny <= 1; ny++) {
                    if (nx === 0 && ny === 0) continue;
                    const cx = mod(ix + nx, gameOfLifeField.length);
                    const cy = mod(iy + ny, gameOfLifeField[0].length);
                    if (gameOfLifeField[cx][cy]) {
                        neighbors++;
                    }
                    if (ix === 1 && iy === 1) {
                        console.log(cx + " " + cy);
                        console.log(gameOfLifeField[cx][cy]);
                    }
                }
            }
            newField[ix][iy] = (gameOfLifeField[ix][iy] === false && neighbors === 3) ||
                (gameOfLifeField[ix][iy] === true && neighbors > 1 && neighbors < 4);
            neighborsArray[ix][iy] = neighbors;
        }
    }
    for (var gx = 0; gx < gameOfLifeField.length; gx++) {
        for (var gy = 0; gy < gameOfLifeField[0].length; gy++) {
            if (!gameOfLifeField[gx][gy]) continue;
            var rectangle = new Rectangle(new Point(gx * 10, gy * 10), new Point(gx * 10 + 10, gy * 10 + 10));
            var path = new Path.Rectangle(rectangle);
            path.fillColor = "#000000";
            path.fillColor = new Color(0.5 - neighborsArray[gx][gy] / 10);
        }
    }
    gameOfLifeField = newField;
}